/******************************/
var Grupo = function(
  _clave,
  _nombre,
  _docente,
  _materias,
  _alumnos
  ){
    return{
      "clave":_clave,
      "nombre":_nombre,
      "docente":_docente,
      "materias":_materias,
      "alumnos":_alumnos
    };
  };
  
  var Docente = function(
    _clave,
    _nombre,
    _apellidos,
    _grado_academico
    ){
      return{
        "clave":_clave,
        "nombre":_nombre,
        "apellidos":_apellidos,
        "grado_academico":_grado_academico
      };
    };
  
  var Alumno = function(
    _clave,
    _nombre,
    _apellidos,
    _calificaciones
    ){
      return{
        "clave":_clave,
        "nombre":_nombre,
        "apellidos":_apellidos,
        "calificaciones":_calificaciones
      };
    };
    
  var Materia = function(
    _clave,
    _nombre
    ){
      return{
        "clave":_clave,
        "nombre":_nombre
      };
    };
  
  /*************************************/
  
  
  /*Funciones!!*/
  /*************************************/
  
  
  function existeMateriaenGrupo(g,cm){
  if(!g.materias || g.materias.length === 0 ){
    return false;
  }
  for(var i = 0; i < g.materias.length; i++){
    if(g.materias[i].clave === cm){
      return true;
    }
  }
  return false;
}
  
  
  function asignarMateriaGrupo(g,m){
  if(!g.materias){
    g.materias = [];
  }
  if(!existeMateriaenGrupo(g,m.clave)){
  g.materias.push(m);
  return true;
  }
  return false;
}
  
  function asignarDocente(g,d){
    if(!g.docente)
    {
      g.docente = d;
      return {
        "exito": true,
        "mensaje":"docente asignado al grupo " + g.nombre
        };
    }
    return {
      "exit":false,
      "mensaje":"el grupo " + g.nombre + " ya tiene docente asignado"
      };
  }
  
  function docenteAsignado(g, cd)
  {
    if(g.docente.clave === cd){
      return true;
    }
    return false;
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  